<html lang="en">

<head>

<meta charset="utf-8">

<title>Sfeed: HTML/CSS standard for enabling feeds</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<style>

/*******
 * TAGS
 * *****/

html {
	margin: 0 0.5rem;
}

main {
	margin: 1rem auto;
	width: 44rem;
}

h1 {

	border-bottom: 1px solid #000;
	padding-bottom: 1rem;
}

h3 {
	font-family: sans-serif;
}

a {
	color: maroon;
	font-weight: 600;
	text-decoration: none;
}

p {
	line-height: 1.25;
}

pre {
	background: #222;
	border-radius: 0.5rem;
	color: lime;
	padding: 1rem;
	tab-size: 5;
	white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
	white-space: -o-pre-wrap; /* Opera 7 */
	white-space: -pre-wrap; /* Opera 4-6 */
	white-space: pre-wrap; /* css-3 */
	word-wrap: break-word; /* Internet Explorer 5.5+ */
}

code {
	background: #ddd;
	border-radius: 0.125rem;
	padding: 0.05rem 0.125rem;
}

/*******
 * CLASSES
 * *****/

.f {
	font-size: 115%;
	font-style: italic;
}

.datetime {
	border-top: 1px solid #000;
	font-style: italic;
	padding-top: 1rem;
	text-align: right;
}

.changelog {
	background: #A770EF; /* fallback for old browsers */
	background: -webkit-linear-gradient(to right, #FDB99B, #CF8BF3, #A770EF); /* Chrome 10-25, Safari 5.1-6 */
	background: linear-gradient(to right, #FDB99B, #CF8BF3, #A770EF); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	border-radius: 0.25rem;
	color: #fff;
	float: right;
	font-family: sans-serif;
	font-size: 85%;
	margin-bottom: 1rem;
	margin-left: 1rem;
	margin-right: -12.5%;
	min-width: 16rem;
	padding-bottom: 0.5rem;
	padding-left: 0.75rem;
	padding-right: 0.75rem;
	width: 25%;
}

.changelog ul {
	padding-left: 0;
	list-style: none;
	margin-bottom: 0.75rem;
}

.changelog ul ul {
	padding-left: 1rem;
	list-style: disc;
}

.changelog li label {
	font-family: serif;
	font-style: italic;
	margin-bottom: 1rem;
}

.preface p {
	font-style: italic;
}

.email {
	font-style: italic;
}

/*********
 * MEDIA QUERIES
 *********/

@media screen and (max-width: 812px) {

	main {
		width: 100%;
		margin: 0;
	}

	h1 {
		font-size: 150%;
	}

	.changelog {
		border-bottom-right-radius: 0;
		border-top-right-radius: 0;
		margin-right: -1rem;
		min-width: 6rem;
	}

}

</style>

<body>

<main>

<h1>
	s<span class="f">f</span>eed, an HTML/CSS standard for enabling feeds
</h1>

<aside class="changelog">

	<h3>
		Changelog
	</h3>

	<ul>

		<li>
			<label>
				2021 Jul 9
			</label>

			<ul>
				<li>
					Added Preface
				</li>
			</ul>

		</li>

		<li>
			<label>
				2021 Jul 5
			</label>
			<ul>
				<li>
					Initial post
				</li>
			</ul>

		</li>

	</ul>

</aside>

<div class="preface">

	<h3>
		Preface
	</h3>

	<p>
		After linking to this page on the Hacker News thread <a href="https://news.ycombinator.com/item?id=27739568">Show HN: RSS feeds for arbitrary websites using CSS selectors</a>, a couple of swift replies noted that s<span class="f">f</span>eed already exists as the <a href="https://microformats.org/wiki/h-feed"> h-feed</a> microformat!
	</p>

	<p>
		Perhaps because embracing the JSON Feed standard seems so intuitive to me, I still don't quite get the h-feed spec, even after reading it a couple of times. If after studying it further I still believe s<span class="f">f</span>eed contains better ideas, I will contact the maintainers of the microformat with a request for changes.
	</p>

	<p>
		Also, this microformat needs to be promoted! The <a href="https://en.wikipedia.org/wiki/Web_standards">web standards</a> movement persuaded both browser vendors and web designers alike to standardize; it should be easier to persuade feed reader developers and web developers to do so today. The societal stakes are clearly higher, the technical implementation much easier.
	</p>

	<p>
		—Adam
	</p>

</div>

<h3>
	Rationale
</h3>

<p>
	Web feeds derived from documents are a great contribution to the Web, enabling valuable timely sharing. They should be as easy as possible to produce.
</p>

<p>
	Since 1999 however we’ve produced them by duplicating the contents of an HTML page into an accompanying XML file to create an RSS/Atom feed (and more recently into JSON for a JSON Feed). Even as CMSs such as WordPress undertook the task of producing these RSS feeds automatically, the method is unnecessarily redundant (two versions of each page) and back-to-front, optimizing for the relatively few feed readers rather than for the multitudinous producers of content pages.
</p>

<p>
	Instead the following is a simple spec to specify the elements of a feed from <em>within</em> HTML. The Web page can then be consumed directly by a feed reader empowered with s<span class="f">f</span>eed functionality without the painstaking configuration of a scraping setup per selector per feed, nor the need for the content producer to create and maintain any additional files.
</p>

<p>
	Pressing this need is that newer Javascript-powered publishing stacks do not easily produce XML, so that many recent blogs — even within the web development sphere itself — lack RSS feeds entirely. And we don’t want that.
</p>

<h3>
	The Elements as Selectors
</h3>

<p>
	The general method of the spec is to use CSS classes and/or HTML attribs to express the <a href="https://jsonfeed.org">JSON Feed</a> standard (because it’s fresher than RSS and Atom). Please refer to the <a href="https://jsonfeed.org/version/1.1">JSON Feed docs</a> for rationale behind each of these elements. Following the order set by JSON Feed, they are, as CSS classes:
</p>

<pre>
class="-sfeed-sfeed[.*]"
class="-sfeed-top-version"
class="-sfeed-top-title"
class="-sfeed-top-home_page_url"
class="-sfeed-top-description"
class="-sfeed-top-user_comment"
class="-sfeed-top-next_url"
class="-sfeed-top-icon"
class="-sfeed-top-favicon"
class="-sfeed-top-authors"
class="-sfeed-top-author-name"
class="-sfeed-top-author-url"
class="-sfeed-top-author-avatar"
class="-sfeed-top-language"
class="-sfeed-top-expired"
class="-sfeed-top-hubs"
class="-sfeed-item"
class="-sfeed-id"
class="-sfeed-url"
class="-sfeed-external_url"
class="-sfeed-title"
class="-sfeed-content_html"
class="-sfeed-content_text"
class="-sfeed-summary"
class="-sfeed-image"
class="-sfeed-banner_image"
class="-sfeed-date_published"
class="-sfeed-date_modified"
class="-sfeed-authors"
class="-sfeed-author-name"
class="-sfeed-author-url"
class="-sfeed-author-avatar"
class="-sfeed-tags"
class="-sfeed-language"
class="-sfeed-attachments"
class="-sfeed-attachment-url"
class="-sfeed-attachment-mime_type"
class="-sfeed-attachment-title"
class="-sfeed-attachment-size_in_bytes"
class="-sfeed-attachment-duration_in_seconds"

</pre>

And as HTML attributes:

<pre>
sfeed="sfeed[.*]"
sfeed="top-title"
sfeed="top-home_page_url"
...
sfeed="attachment-duration_in_seconds"
</pre>

<h3>
	Notes on Element Selectors
</h3>

<p>
	The elements <code>sfeed[.*]</code> and <code>item</code> are not taken from the JSON Feed spec; instead they are how we focus on our data within the larger HTML page. Conversely the JSON Feed element <code>feed_url</code> is now unnecessary so excluded.
</p>

<p>
	The <code>.-sfeed-sfeed[.*]</code> element is allowed further signifiers, (eg, "sfeed-1", "sfeed-2", "sfeed-most-emailed") in order to facilitate more than one feed per URL.
</p>

<p>
	Top-level fields such as <code>.-sfeed-top-description</code> are not normally necessary as the page’s metatags serve this purpose; they should only be used when setting up multiple sfeeds on a single page, ie, when the page’s meta-data no longer match the feed’s top-level fields.
</p>

<p>
	This explains why the prefix "top-" is added to top-level fields rather than "item-" to item-level: the latter are more likely to be in use, justifying the more concise labeling.
</p>

<p>
	One problem when scraping is when the item’s link, its <code>a</code> tag, is used as the item container. Here, add both classes:
</p>

<pre>
class="-sfeed-item -sfeed-url"
</pre>

<p>
	Or as an attribute:
</p>

<pre>
sfeed="item+url"
</pre>

<h3>
	Activation
</h3>

<p>
	A page’s s<span class="f">f</span>eed functionality is activated (made s<span class="f">f</span>eedy?) via a metatag:
</p>

<pre>
&lt;meta property="sfeed" /&gt;
</pre>

<p>
	There seems no need for a value here, it’s just a trigger.
</p>

<p>
	In the unlikely event that some or all the pertinent HTML is not editable so that neither s<span class="f">f</span>eedy CSS classes nor HTML attributes can be added, then the key to the actual CSS selectors can be placed either in meta tags or an external file (inline though is preferred as it is simpler and only a single feed per page can be generated using this method). As meta tags:
</p>

<pre>
&lt;meta property="sfeed:sfeed" content="my-selector" /&gt;
&lt;meta property="sfeed:item" content="my-selector" /&gt;
&lt;meta property="sfeed:id" content="my-selector" /&gt;
...
&lt;meta property="sfeed:attachment-duration_in_seconds" content="my-selector" /&gt;
</pre>

<p>
	And as a link to an external JSON file:
</p>

<pre>
&lt;link type="application/sfeed+json" href="https://example.org/[my-sfeed-file].json" /&gt;
</pre>

<p>
	In the external file, each pair is comprised of the element and a CSS class name. A pair should only be included if its corresponding element exists on the web page. If a pair contains an empty value, then the element is specified on the web page and should be sought there.
</p>

<p>
	An example <code>sfeed.json</code> file:
</p>

<pre>
{
	"sfeed": "my-selector",
	"top-title": "my-selector",
	"top-home_page_url": "my-selector",
	...
	"attachment-duration_in_seconds": "my-selector"
}
</pre>

<p>
	s<span class="f">f</span>eedback is very welcome, especially from feed reader developers, whom I hope will be interested in building functionality to handle the s<span class="f">f</span>eed standard; and from web designers and developers, without whose embrace it achieves nothing.
</p>

<p>
	Please email me, Adam Khan, at <a href="mailto:adam@engaging.net" class="email">adam@engaging.net</a> or tweet <a href="https://twitter.com/hashtag/sfeed">#sfeed</a>.
</p>

<p class="datetime">
	Lasted edited: Tuesday, July 9th, 2021, 13:18 UTC<br>
	First posted: Monday, July 5th, 2021
</p>

</main>

</body>

</html>